desc "Import menus." 
task :import_menus => :environment do
  File.open("menu.txt", "r").each do |line|
  Restaurant_id, Item_id, price = line.strip.split(",")
    r = Menu.new(:Restaurant_id => Restaurant_id, :Item_id => Item_id, :price => price)
   r.save
  end
end
