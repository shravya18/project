json.array!(@restaurants) do |restaurant|
  json.extract! restaurant, :id, :rid, :rname, :phno, :delivery, :rating, :timings, :category
  json.url restaurant_url(restaurant, format: :json)
end
