json.array!(@menus) do |menu|
  json.extract! menu, :id, :rid, :itemid, :price
  json.url menu_url(menu, format: :json)
end
