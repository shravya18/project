json.array!(@items) do |item|
  json.extract! item, :id, :itemid, :iname
  json.url item_url(item, format: :json)
end
