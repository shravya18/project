json.array!(@locations) do |location|
  json.extract! location, :id, :rid, :streetno, :streetname, :zip, :city, :state
  json.url location_url(location, format: :json)
end
