class UsersController < ApplicationController
def new
@user = User.new
end

def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :name, :zip, :city, :state)
end

def create
@user = User.new(user_params)
if @user.save
redirect_to root_url, :notice => "Successfully
signed up!"
else
render "new"
end
end
end
