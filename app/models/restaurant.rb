class Restaurant < ActiveRecord::Base
has_one :location
has_many :menus, dependent: :destroy
has_many :items, :through => :menus
#def self.query(query)
    # where(:title, query) -> This would return an exact match of the query
    #binding.pry
    #Restaurant.joins(:location).where("locations.zip = ?", "%#{query}")
    #where("rating like ?", "%#{query}%")

    #Restaurant.find_by_sql("SELECT * from restaurants INNER JOIN locations ON restaurants.id = locations.restaurant_id where locations.zip = '87102' ") 
  #end 
end
