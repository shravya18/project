class User < ActiveRecord::Base
has_secure_password
#attr_accessible :email, :password_digest, :password,
#:password_confirmation, :name, :zip, :city, :state
validates :email, :presence => true, :uniqueness =>
{:case_sensitive => false}, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
validates :name, :presence => true, length: { maximum: 30 }
validates :zip, :presence => true, numericality: true
validates :city, :presence => true
validates :state, :presence => true
end
