class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :itemid
      t.string :iname

      t.timestamps
    end
  end
end
