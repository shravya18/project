class CreateRestaurants < ActiveRecord::Migration
  def change
    create_table :restaurants do |t|
      t.integer :rid
      t.string :rname
      t.string :phno
      t.string :delivery
      t.integer :rating
      t.string :timings
      t.string :category

      t.timestamps
    end
  end
end
