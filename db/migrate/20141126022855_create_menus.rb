class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.belongs_to :restaurant
      t.belongs_to :item
      t.string :price

      t.timestamps
    end
  end
end
