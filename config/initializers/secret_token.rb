# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Bunny::Application.config.secret_key_base = 'a07e42d8a3f6436cf2115f581cfd4f269d180568ff55914c40ac96c837f88ef7a977779b45461551e220d52212836892f6c559d4700092b45d8f0c701e679cf2'
